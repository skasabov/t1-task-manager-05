package ru.t1.skasabov.tm;

import static ru.t1.skasabov.tm.constant.TerminalConst.*;

public final class Application {

    public static void main(String[] args) {
        processArguments(args);
    }

    public static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }

    public static void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case HELP:
                showHelp();
                break;
            case ABOUT:
                showAbout();
                break;
            case VERSION:
                showVersion();
                break;
            default:
                showError();
                break;
        }
    }

    public static void showError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Stas Kasabov");
        System.out.println("email: stas@kasabov.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show developer info.\n", ABOUT);
        System.out.printf("%s - Show application version.\n", VERSION);
        System.out.printf("%s - Show command list.\n", HELP);
    }

}
